package com.golendukhin.gimmegyphy

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.golendukhin.gimmegyphy.data.GifsEntity
import com.golendukhin.gimmegyphy.data.GiphyRepository
import com.golendukhin.gimmegyphy.domain.DomainResponse
import com.golendukhin.gimmegyphy.domain.GetGifsUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString

@ExperimentalCoroutinesApi
class GetGifsUseCaseTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @MockK
    private lateinit var giphyRepository: GiphyRepository

    @MockK
    private lateinit var gifsEntity: GifsEntity

    private lateinit var getGifsUseCase: GetGifsUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        getGifsUseCase = GetGifsUseCase(giphyRepository)
    }

    @Test
    fun `GIVEN I want gifs entity WHEN invoke is called THEN Content is emitted`() {
        testCoroutineRule.runBlockingTest {
            coEvery { giphyRepository(anyString(), anyString(), anyInt(), anyInt()) } answers { DomainResponse.Content(gifsEntity) }
            val result = getGifsUseCase(anyString(), anyString(), anyInt(), anyInt())
            assertEquals(result, DomainResponse.Content(gifsEntity))
        }
    }

    @Test
    fun `GIVEN I want gifs entity WHEN invoke is called AND ann error occurs THEN Error is emitted`() {
        testCoroutineRule.runBlockingTest {
            coEvery { giphyRepository(anyString(), anyString(), anyInt(), anyInt()) } answers { DomainResponse.Error }
            val result = getGifsUseCase(anyString(), anyString(), anyInt(), anyInt())
            assertEquals(result, DomainResponse.Error)
        }
    }
}