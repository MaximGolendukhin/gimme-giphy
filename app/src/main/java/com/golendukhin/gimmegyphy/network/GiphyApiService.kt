package com.golendukhin.gimmegyphy.network

import com.golendukhin.gimmegyphy.data.GifsEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyApiService {
    @GET("v1/gifs/search")
    suspend fun getGifs(
        @Query("api_key") apiKey: String,
        @Query("q") query: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
    ): GifsEntity
}