package com.golendukhin.gimmegyphy.data

import com.golendukhin.gimmegyphy.domain.DomainResponse
import javax.inject.Inject

class GiphyRepository @Inject constructor(private val giphyDataSource: GiphyDataSource) {
    suspend operator fun invoke(
        apiKey: String,
        query: String,
        offset: Int,
        limit: Int,
    ): DomainResponse = DomainResponse.Content(giphyDataSource(apiKey, query, offset, limit))
}