package com.golendukhin.gimmegyphy.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GifsEntity(
    @field:Json(name = "data") val data: List<Gif>,
)

data class Gif(
    @field:Json(name = "images") val images: Image,
)

data class Image(
    @field:Json(name = "fixedWidth") val fixed_width: GifUrl,
)

data class GifUrl(
    @field:Json(name = "url") val url: String,
)