package com.golendukhin.gimmegyphy.data

import com.golendukhin.gimmegyphy.network.GiphyApiService
import javax.inject.Inject

class GiphyDataSource @Inject constructor(private val giphyApiService: GiphyApiService) {
    suspend operator fun invoke(
        apiKey: String,
        query: String,
        offset: Int,
        limit: Int
    ): GifsEntity = giphyApiService.getGifs(apiKey, query, offset, limit)
}