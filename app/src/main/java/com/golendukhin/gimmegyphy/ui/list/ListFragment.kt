package com.golendukhin.gimmegyphy.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.golendukhin.gimmegyphy.R
import com.golendukhin.gimmegyphy.data.Gif
import com.golendukhin.gimmegyphy.databinding.ListFragmentBinding
import com.golendukhin.gimmegyphy.domain.DomainResponse
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*

const val CAN_SCROLL_ITEMS = 10
const val DEBOUNCE_MILLISECONDS = 500L
const val MIN_SYMBOLS_INPUT = 2

@AndroidEntryPoint
class ListFragment : Fragment() {
    private val viewModel: GifsViewModel by activityViewModels()
    private val adapter: GifsAdapter = GifsAdapter(ItemClickListener { viewModel.updateUrl(it) })
    private lateinit var binding: ListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = ListFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        observe()

        with (binding.recyclerView) {
            adapter = this@ListFragment.adapter
            layoutManager = LinearLayoutManager(activity)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0 && !recyclerView.canScrollVertically(CAN_SCROLL_ITEMS)) {
                        viewModel.getGifsEntities(
                            resources.getString(R.string.api_key),
                            binding.textInputEditText.text.toString()
                        )
                    }
                }
            })
        }
        binding.textInputEditText
            .textChanges()
            .debounce(DEBOUNCE_MILLISECONDS)
            .filter {
                it.toString().length > MIN_SYMBOLS_INPUT }
            .map { viewModel.getGifsEntities(
                    resources.getString(R.string.api_key),
                    it.toString(),
                    true
                )
            }
            .launchIn(lifecycleScope)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        return binding.root
    }

    private fun observe() {
        viewModel.gifsEntity.observe(viewLifecycleOwner) {
            val (response, isNew) = it
            when (response) {
                is DomainResponse.Content -> {
                    val mergedList = (if (isNew) emptyList() else adapter.currentList) +
                            response.result.data.toUrlList()
                    adapter.submitList(mergedList)
                    if (mergedList.isEmpty()) showSnackBar()
                    binding.progressBar.isVisible = false
                }
                DomainResponse.Loading ->
                    binding.progressBar.isVisible = true
                DomainResponse.Error -> Unit
            }
        }

        viewModel.url.observe(viewLifecycleOwner) {
            it?.let {
                this
                    .findNavController()
                    .navigate(ListFragmentDirections.actionListFragmentToDetailsFragment(it))
                viewModel.setUrlToNull()
            }
        }
    }

    private fun List<Gif>.toUrlList(): List<String> = this.map { it.images.fixed_width.url }

    @ExperimentalCoroutinesApi
    private fun EditText.textChanges(): Flow<CharSequence?> = callbackFlow {
        val listener = doOnTextChanged { text, _, _, _ -> trySend(text) }
        awaitClose { removeTextChangedListener(listener) }
    }.onStart {
        emit(text) }

    private fun showSnackBar() {
        Snackbar.make(
            binding.listFragment,
            resources.getText(R.string.snack_bar_content),
            Snackbar.LENGTH_LONG
        ).show()
    }
}