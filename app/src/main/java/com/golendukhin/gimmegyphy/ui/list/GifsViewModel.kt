package com.golendukhin.gimmegyphy.ui.list

import androidx.lifecycle.*
import com.golendukhin.gimmegyphy.domain.DomainResponse
import com.golendukhin.gimmegyphy.domain.GetGifsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

const val GIFS_LIMIT = 50

@HiltViewModel
class GifsViewModel @Inject constructor(
    private val getGifsUseCase: GetGifsUseCase
): ViewModel() {
    companion object {
        var requestsMade = 0
    }

    private val _gifsEntity = MutableLiveData<Pair<DomainResponse, Boolean>>()
    val gifsEntity: LiveData<Pair<DomainResponse, Boolean>>
        get() = _gifsEntity

    private val _url = MutableLiveData<String?>()
    val url: LiveData<String?>
        get() = _url

    fun getGifsEntities(apiKey: String, query: String, isNew: Boolean = false) {
        if (isNew) requestsMade = 0
        _gifsEntity.value = DomainResponse.Loading to isNew
        viewModelScope.launch {
            getGifsUseCase(
                apiKey = apiKey,
                query = query,
                offset = GIFS_LIMIT * requestsMade++,
                limit = GIFS_LIMIT,
            ).run { _gifsEntity.value = this to isNew }
        }
    }

    fun updateUrl(url: String) {
        _url.value = url
    }

    fun setUrlToNull() {
        _url.value = null
    }
}