package com.golendukhin.gimmegyphy.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.golendukhin.gimmegyphy.databinding.GifItemBinding

import com.golendukhin.gimmegyphy.ui.list.GifsAdapter.ViewHolder.Companion.from

class GifsAdapter(
    private val itemCLickListener: ItemClickListener
    ) : ListAdapter<String, GifsAdapter.ViewHolder>(GifItemsDiffCallBack()) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val url = getItem(position)
        holder.bind(itemCLickListener, url)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = from(parent)

    class ViewHolder private constructor(private val binding: GifItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(itemClickListener: ItemClickListener, url: String) {
            Glide
                .with(binding.listItemImageView.context)
                .load(url)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(binding.listItemImageView)
            binding.url = url
            binding.clickListener = itemClickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GifItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class GifItemsDiffCallBack : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String) = oldItem == newItem

    override fun areContentsTheSame(oldItem: String, newItem: String) = oldItem == newItem
}

class ItemClickListener(val clickListener: (url: String) -> Unit) {
    fun onClick(url: String) = clickListener(url)
}