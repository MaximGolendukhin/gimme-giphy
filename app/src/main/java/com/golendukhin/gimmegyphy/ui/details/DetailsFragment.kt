package com.golendukhin.gimmegyphy.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.golendukhin.gimmegyphy.databinding.DetailsFragmentBinding

class DetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DetailsFragmentBinding.inflate(inflater, container, false)
        val url: String = DetailsFragmentArgs.fromBundle(requireArguments()).url
        binding.lifecycleOwner = this

        Glide
            .with(this.requireContext())
            .load(url)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
            .into(binding.detailsImageView)

        return binding.root
    }
}