package com.golendukhin.gimmegyphy.domain

import com.golendukhin.gimmegyphy.data.GifsEntity

sealed class DomainResponse {
    object Loading : DomainResponse()
    object Error : DomainResponse()
    data class Content(val result: GifsEntity) : DomainResponse()
}