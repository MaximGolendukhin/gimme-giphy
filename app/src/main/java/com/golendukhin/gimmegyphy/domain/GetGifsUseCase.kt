package com.golendukhin.gimmegyphy.domain

import com.golendukhin.gimmegyphy.data.GiphyRepository
import javax.inject.Inject

class GetGifsUseCase @Inject constructor(private val giphyRepository: GiphyRepository) {
    suspend operator fun invoke(
        apiKey: String,
        query: String,
        offset: Int,
        limit: Int
        ): DomainResponse = giphyRepository(apiKey, query, offset, limit)
}