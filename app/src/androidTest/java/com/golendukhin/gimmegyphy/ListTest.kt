package com.golendukhin.gimmegyphy

import android.os.SystemClock
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ListTest {
    @get:Rule
    val mActivityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun whenTypedTwoSymbols_thenProgressBarIsNotShown() {
        onView(withId(R.id.text_input_edit_text)).perform(typeText("aa"))
        onView(withId(R.id.progress_bar)).check(matches(not(isDisplayed())))
    }

    @Test
    fun whenTypedTwoSymbols_thenGifsListIsNotShown() {
        onView(withId(R.id.text_input_edit_text)).perform(typeText("aa"))
        onView(withId(R.id.recycler_view)).check(matches(recyclerViewSizeMatcher(0)))
    }

    @Test
    fun whenTypedThreeSymbols_thenProgressBarIsShown()  {
        onView(withId(R.id.text_input_edit_text)).perform(typeText("aaa"))
        SystemClock.sleep(600)
        onView(withId(R.id.progress_bar)).check(matches(isDisplayed()))
    }

    private fun recyclerViewSizeMatcher(matcherSize: Int): Matcher<View?>? {
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("with list size: $matcherSize")
            }

            override fun matchesSafely(recyclerView: RecyclerView): Boolean {
                return matcherSize == recyclerView.adapter!!.itemCount
            }
        }
    }
}